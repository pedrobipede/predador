package br.com.maincore.predador.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maincore.predador.model.Drone;

public interface DroneRepository extends JpaRepository<Drone, Long>{

}
