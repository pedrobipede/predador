package br.com.maincore.predador.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maincore.predador.model.Coordenada;

public interface CoordenadaRepository extends JpaRepository<Coordenada, Long>{

}
