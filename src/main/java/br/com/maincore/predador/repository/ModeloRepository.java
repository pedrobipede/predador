package br.com.maincore.predador.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maincore.predador.model.Modelo;

public interface ModeloRepository extends JpaRepository<Modelo, Long>{

}
