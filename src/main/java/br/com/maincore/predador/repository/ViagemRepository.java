package br.com.maincore.predador.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maincore.predador.model.Viagem;

public interface ViagemRepository extends JpaRepository<Viagem, Long>{

}
