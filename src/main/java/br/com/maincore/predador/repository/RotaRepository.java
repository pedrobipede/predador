package br.com.maincore.predador.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.maincore.predador.model.Rota;

public interface RotaRepository extends JpaRepository<Rota, Long>{

}
