package br.com.maincore.predador.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;

/**
 * Created by roberto.klein on 9/18/18.
 */
@Component
public class JwtGenerator {

    @Value("${app.jwtSecret}")
    private String secret;

    @Value("${app.jwtExpirationInMs}")
    private long expires;

    public String generate(JwtUser jwtUser) {

        Claims claims = Jwts.claims()
                .setSubject(jwtUser.getUserName());
        claims.put("userId", (String.valueOf(jwtUser.getId())));
        claims.put("role", jwtUser.getRole());

        Instant expirationTime = Instant.now()
                .plusSeconds(expires);

        Date expirationDate = Date.from(expirationTime);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .setExpiration(expirationDate)
                .compact();
    }
}
