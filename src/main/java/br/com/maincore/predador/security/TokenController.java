package br.com.maincore.predador.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maincore.predador.model.Credentials;
import br.com.maincore.predador.model.Usuario;
import br.com.maincore.predador.repository.UsuarioRepository;

@RestController
@RequestMapping("/login/auth")
public class TokenController {

    private JwtGenerator jwtGenerator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsuarioRepository userRepository;

    public TokenController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping
    public ResponseEntity<?> validate(@RequestBody final Credentials credentials) {

    	System.out.println("credencial: " + credentials.getPassword());
        String username = credentials.getUsername();
        Optional<Usuario> userDB = userRepository.findByUsername(username);

        JwtToken token = null;

        System.out.println("credencial: " + credentials.getPassword());

     //   String encodedPass = passwordEncoder.encode(credentials.getPassword());

        System.out.println("username:   "+credentials.getUsername());
        System.out.println("pass: " + credentials.getPassword());

        System.out.println(userDB.get().toString());
        if(userDB.isPresent()) {
            if (credentials.getPassword().equals(userDB.get().getPassword()) && credentials.getUsername().equals(userDB.get().getUsername()) ) {
                JwtUser jwtUser = new JwtUser();
                jwtUser.setId(userDB.get().getId());
                jwtUser.setUserName(userDB.get().getNome());

                if (userDB.get().isAdmin()) {
                    jwtUser.setRole("ROLE_ADMIN");
                }

                token = new JwtToken(jwtGenerator.generate(jwtUser));
                return new ResponseEntity<>(token, null, HttpStatus.CREATED);
            }
        }
        return new ResponseEntity<>(token, null, HttpStatus.CONFLICT);
    }

}
