package br.com.maincore.predador.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maincore.predador.model.Coordenada;
import br.com.maincore.predador.model.Viagem;
import br.com.maincore.predador.response.Response;
import br.com.maincore.predador.service.DroneService;
import br.com.maincore.predador.service.ViagemService;

@RestController
@RequestMapping("/api/viagem")
public class ViagemController {

	@Autowired
	private ViagemService service;
	@Autowired
	private DroneService droneService;

	@RequestMapping("/viagensResponse")
	public ResponseEntity<?> oi() {
		
		Response<Coordenada> response = new Response<Coordenada>();
		response.setData(new Coordenada("-25.5054701","-54.5712808"));
		return ResponseEntity.ok(response);
	}

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Viagem entity) {
		try {

			Viagem body = service.save(entity);

			return ResponseEntity.ok(body);
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(this.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		try {

			return ResponseEntity.ok(service.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(service.findById(id));
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
}
