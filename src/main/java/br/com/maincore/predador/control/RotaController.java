package br.com.maincore.predador.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maincore.predador.model.Coordenada;
import br.com.maincore.predador.model.Rota;
import br.com.maincore.predador.service.RotaService;

@RestController
@RequestMapping("/api/rotas")
public class RotaController {

	@Autowired
	private RotaService service;

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Rota entity) {
		try {

			Rota body = service.save(entity);

			return ResponseEntity.ok(body);
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(this.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		try {

			return ResponseEntity.ok(service.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(service.findById(id));
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	
	@RequestMapping("/salvar")
	public ResponseEntity<?> salvar(@RequestBody Rota rota){
		
		System.out.println(rota.toString());
		System.out.println(this.service.save(rota));
		
		return ResponseEntity.ok(null);
	}
}
