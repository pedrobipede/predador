package br.com.maincore.predador.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maincore.predador.dto.LoginDto;
import br.com.maincore.predador.dto.UsusarioDto;
import br.com.maincore.predador.model.Usuario;
import br.com.maincore.predador.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	@RequestMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginDto login){
		
		System.out.println(login.toString());
		return ResponseEntity.ok(new LoginDto()); 
	}
 
	@RequestMapping("/salvar")
	public ResponseEntity<?> save(@RequestBody UsusarioDto entity) {
		System.out.println("opi");
		System.out.println(entity.getNome());
		System.out.println(converDto(entity).toString());
		Usuario usuario = converDto(entity);
		try {
			return ResponseEntity.ok(this.service.save(usuario));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ResponseEntity.ok(entity);
		
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(this.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		try {

			return ResponseEntity.ok(service.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(service.findById(id));
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	
	
	private Usuario converDto(UsusarioDto dto) {
		Usuario us = new Usuario();
		
		us.setCelular(dto.getCelular());
		us.setCpf(dto.getCpf());
		us.setEndereco(dto.getEndereco());
		us.setUsername(dto.getLogin());
		us.setNome(dto.getNome());
		us.setPassword(dto.getSenha());
		us.setTipo(dto.getTipo());
		
		return us;
	}
}
