package br.com.maincore.predador.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.maincore.predador.model.Coordenada;
import br.com.maincore.predador.model.Drone;
import br.com.maincore.predador.response.Response;
import br.com.maincore.predador.service.DroneService;

@RestController
@RequestMapping("/api/drones")
public class DroneController {

	@Autowired
	private DroneService service;
	

	@PostMapping("/")
	public ResponseEntity<?> save(@RequestBody Drone entity) {
		try {
			System.out.println("oiiiii");
			Drone body = service.save(entity);

			return ResponseEntity.ok(body);
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	@RequestMapping("/localizacaoAtual")
	public ResponseEntity<?> oi() {
		
		Response<Coordenada> response = new Response<Coordenada>();
		response.setData(new Coordenada("-25.50570249773988"," -54.63642629255372"));
		return ResponseEntity.ok(response);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") long id) {
		try {
			System.out.println("oi");
			service.delete(id);
			
			return ResponseEntity.ok(this.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@GetMapping("/")
	public ResponseEntity<?> findAll() {
		System.out.println("oi");
		try {
			return ResponseEntity.ok(service.findAll());
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		try {

			return ResponseEntity.ok(service.findById(id));
		} catch (InternalError | Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
}
