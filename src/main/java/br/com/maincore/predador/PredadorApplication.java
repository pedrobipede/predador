package br.com.maincore.predador;

import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import br.com.maincore.predador.mqtt.Subscriber;
import br.com.maincore.predador.repository.UsuarioRepository;

@SpringBootApplication
@EnableWebSecurity
@EnableCaching
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled= true, securedEnabled = true)
public class PredadorApplication {
	
	@Autowired
	private UsuarioRepository re;
	public static void main(String[] args) throws MqttSecurityException {
		SpringApplication.run(PredadorApplication.class, args);
		
		String host = "tcp://192.168.1.11:1883";
		String clientId = "MQTT-todesesperado";

//		String topic = "maincore/sensor/#";
		Subscriber sub = new Subscriber();

		sub.connect(host, clientId);

	}
	
//	@Bean
//	public CommandLineRunner run() {
//		return run ->{
//			
//			try {
//				Usuario us = new Usuario();
//			
//			us.setusername("root");
//			us.setpassword("123456");
//			us.setCpf("11111111111");
//			us.setAdmin(true);
//			us.setNome("eu");
//			us.setTipo(1);
//			
//			us.getRoles().add(new Role(null));
//			
//			us.setAdmin(true);
//			
//			re.save(us);
//			} catch (Exception e) {
//				System.err.println(e.getMessage());
//			}
//			
//			
//		};
//	}
}
