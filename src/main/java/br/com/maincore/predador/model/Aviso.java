package br.com.maincore.predador.model;

public class Aviso {

	private String codigo;
	private long droneid;
	private long rotaid;
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public long getDroneid() {
		return droneid;
	}

	public void setDroneid(long droneid) {
		this.droneid = droneid;
	}

	public long getRotaid() {
		return rotaid;
	}

	public void setRotaid(long rotaid) {
		this.rotaid = rotaid;
	}

	@Override
	public String toString() {
		return "[codigo:"+this.getCodigo()+", droneid:"+this.getDroneid()+", rotaid:"+this.getRotaid()+"]";  
	}
}
