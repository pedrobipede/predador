package br.com.maincore.predador.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "tbmodelo")
public class Modelo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String descricao;
	private String nome;
	private String produtora;

	@CreatedDate
	private Date created;

	@OneToMany(mappedBy = "modelo")
	private List<Drone> drones;

	public Modelo() {
	}

	public Modelo(String descricao, String nome, String produtora, Date created, List<Drone> drones) {
		this.descricao = descricao;
		this.nome = nome;
		this.produtora = produtora;
		this.created = created;
		this.drones = drones;
	}

	public Modelo(long id, String descricao, String nome, String produtora, Date created, List<Drone> drones) {
		this.id = id;
		this.descricao = descricao;
		this.nome = nome;
		this.produtora = produtora;
		this.created = created;
		this.drones = drones;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getProdutora() {
		return produtora;
	}

	public void setProdutora(String produtora) {
		this.produtora = produtora;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public List<Drone> getDrones() {
		return drones;
	}

	public void setDrones(List<Drone> drones) {
		this.drones = drones;
	}

}
