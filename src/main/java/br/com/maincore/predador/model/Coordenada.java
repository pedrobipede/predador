package br.com.maincore.predador.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbcoordenada")
public class Coordenada {
//	https://www.callicoder.com/hibernate-spring-boot-jpa-many-to-many-mapping-example/

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

//	@Column(nullable = false)
	private String latitude;

//	@Column(nullable = false)
	private String longitude;

//	@Column(nullable = false)
	private String ponto;

	/*
	 * @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
	 * CascadeType.MERGE })
	 * 
	 * @JoinTable(name = "tbcoordenadarota", joinColumns = { @JoinColumn(name =
	 * "coordenada_id") }, inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "rota_id") }) private List<Rota> rotas;
	 */

//	@ManyToOne(fetch = FetchType.LAZY, optional = false)
//	@JoinColumn(name = "ROTA_ID", referencedColumnName = "ID")
//	@JoinColumn(name="clll")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "ROTA_ID", referencedColumnName = "ID")})
	private Rota rota;

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public Coordenada() {
	}

	public Coordenada(long id, String latitude, String longitude, String ponto, List<Rota> rotas) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ponto = ponto;
	}

	public Coordenada(String latitude, String longitude, String ponto, List<Rota> rotas) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.ponto = ponto;
	}

	public Coordenada(String latitude, String longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPonto() {
		return ponto;
	}

	public void setPonto(String ponto) {
		this.ponto = ponto;
	}

	@Override
	public String toString() {
		return "Coordenada [id=" + id + ", latitude=" + latitude + ", longitude=" + longitude + ", ponto=" + ponto
				+ ", rota=" + rota + "]";
	}

	/*
	 * public List<Rota> getRotas() { return rotas; }
	 * 
	 * public void setRotas(List<Rota> rotas) { this.rotas = rotas; }
	 */

}
