package br.com.maincore.predador.model;

import java.sql.Date;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "tbviagem")
public class Viagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@CreatedDate
	private Date created;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "drone_id")
	private Drone drone;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rota_id")
	private Rota rota;

	public Viagem() {
	}

	public Viagem(Date created, Drone drone, Rota rota) {
		this.created = created;
		this.drone = drone;
		this.rota = rota;
	}

	public Viagem(long id, Date created, Drone drone, Rota rota) {
		this.id = id;
		this.created = created;
		this.drone = drone;
		this.rota = rota;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

}
