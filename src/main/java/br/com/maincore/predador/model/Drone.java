package br.com.maincore.predador.model;

import java.sql.Date;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "tbdrone")
public class Drone {

//	{"id":"","nome":"tste","serie":"raqwerqwe","modelo":{"id":3,"descricao":"teste","nome":"teste","produtora":"teste","created":null}}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@CreatedDate
	private Date created;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelo_id")
	private Modelo modelo;

	@Column(nullable = false)
	private String nome;

	private String serie;

	public Drone() {}

	public Drone(Date created, Modelo modelo, String nome, String serie) {
		this.created = created;
		this.modelo = modelo;
		this.nome = nome;
		this.serie = serie;
	}

	public Drone(long id, Date created, Modelo modelo, String nome, String serie) {
		this.id = id;
		this.created = created;
		this.modelo = modelo;
		this.nome = nome;
		this.serie = serie;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

}
