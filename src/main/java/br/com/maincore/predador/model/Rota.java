package br.com.maincore.predador.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "tbrota")
public class Rota {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@CreatedDate
	private Date created;

	//@Column(nullable = false)
	private String nome;

	//@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "rotas")
	@OneToMany(mappedBy = "rota", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Coordenada> coordenadas;

/*	@OneToMany(mappedBy = "rota")
	private List<Viagem> viagens;
*/
	public Rota() {
	}

	public Rota(Date created, String nome, List<Coordenada> coordenadas, List<Viagem> viagens) {
		this.created = created;
		this.nome = nome;
		this.coordenadas = coordenadas;
		//this.viagens = viagens;
	}

	public Rota(long id, Date created, String nome, List<Coordenada> coordenadas, List<Viagem> viagens) {
		this.id = id;
		this.created = created;
		this.nome = nome;
		this.coordenadas = coordenadas;
//		this.viagens = viagens;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Coordenada> getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(List<Coordenada> coordenadas) {
		this.coordenadas = coordenadas;
	}

//	public List<Viagem> getViagens() {
//		return viagens;
//	}
//
//	public void setViagens(List<Viagem> viagens) {
//		this.viagens = viagens;
//	}

	@Override
	public String toString() {
		return "Rota [id=" + id + ", created=" + created + ", nome=" + nome + ", coordenadas=" + coordenadas
				+ "]";
	}
	

}
