package br.com.maincore.predador.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

//https://diyprojects.io/mqtt-mosquitto-communicating-connected-objects-iot/#.W9Lp42hKiUk

//SERVIDOR MOSQUITTO INSTALADO NO LINUX COM OS SEGUINTES COMANDOS:
//$ sudo apt update
//$ sudo apt-get install mosquitto
//$ sudo apt-get install mosquitto-clients

//TÓPICO PUBLICADO PARA TESTES COM O SEGUINTE COMANDO:
//$ mosquitto_pub -h ${idServidor} -t maincore/sensor/teste -m 11.3

public class Subscriber implements MqttCallback {

	private String topic = "maincore/sensor/#";
	private static MqttClient client;
//	String host = "tcp://test.mosquitto.org:1883";
	String host = "tcp://192.168.1.11:1883";

	public void connect(String host, String clientId) throws MqttSecurityException {
		try {
			client = new MqttClient(host, clientId, new MemoryPersistence());

			client.setCallback(this);

			client.connect();
			
			System.out.println("conexão estabelecida com sucesso");
			
			client.subscribe(topic, 2);

		} catch (MqttException ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println(cause);
		System.out.println("stack " + cause.getMessage());
		cause.printStackTrace();
		System.out.println("conexão perdida");

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {

		String mensagem = new String(message.getPayload());
		System.out.println(mensagem);

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

}
