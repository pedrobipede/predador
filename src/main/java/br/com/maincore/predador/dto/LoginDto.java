package br.com.maincore.predador.dto;

public class LoginDto {
	
	private String email;
	private String senha;
	
	
	public LoginDto(String email, String senha2) {
		this.email = email;
		this.senha = senha2;
	}
	public LoginDto() {
		super();
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	@Override
	public String toString() {
		return "LoginDto [email=" + email + ", senha=" + senha + "]";
	}
	
	
}
