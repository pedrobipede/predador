package br.com.maincore.predador.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maincore.predador.model.Coordenada;
import br.com.maincore.predador.repository.CoordenadaRepository;

@Service
public class CoordenadaService {
	
	@Autowired
	private CoordenadaRepository repository;
	
	public Coordenada save(Coordenada entity){
		try {
			return repository.save(entity);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void delete(long id){
		try {
			repository.deleteById(id);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Coordenada> findAll(){
		try {
			return repository.findAll();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Coordenada findById(long id){
		try {
			Optional<Coordenada> optional = repository.findById(id);
			
			return optional.get();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
