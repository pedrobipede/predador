package br.com.maincore.predador.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maincore.predador.model.Modelo;
import br.com.maincore.predador.repository.ModeloRepository;

@Service
public class ModeloService {
	
	@Autowired
	private ModeloRepository repository;
	
	public Modelo save(Modelo entity){
		try {
			return repository.save(entity);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	
	public void delete(long id){
		try {
			repository.deleteById(id);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Modelo> findAll(){
		try {
			return repository.findAll();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Modelo findById(long id){
		try {
			Optional<Modelo> optional = repository.findById(id);
			
			return optional.get();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
