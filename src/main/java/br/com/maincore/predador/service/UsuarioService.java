package br.com.maincore.predador.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maincore.predador.model.Usuario;
import br.com.maincore.predador.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	public Usuario save(Usuario entity){
		try {
			return repository.save(entity);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void delete(long id){
		try {
			repository.deleteById(id);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Usuario> findAll(){
		try {
			return repository.findAll();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Usuario findById(long id) {
		try {
			Optional<Usuario> us = repository.findById(id);
			
			return us.get();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
