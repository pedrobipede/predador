package br.com.maincore.predador.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maincore.predador.model.Viagem;
import br.com.maincore.predador.repository.ViagemRepository;

@Service
public class ViagemService {
	
	@Autowired
	private ViagemRepository repository;
	
	public Viagem save(Viagem entity){
		try {
			return repository.save(entity);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void delete(long id){
		try {
			repository.deleteById(id);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Viagem> findAll(){
		try {
			return repository.findAll();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public Viagem findById(long id){
		try {
			Optional<Viagem> optional = repository.findById(id);
			
			return optional.get();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
