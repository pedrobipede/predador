package br.com.maincore.predador.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.maincore.predador.model.Drone;
import br.com.maincore.predador.repository.DroneRepository;

@Service
public class DroneService {
	
	@Autowired
	private DroneRepository repository;
	
	public Drone save(Drone entity){
		try {
			return repository.save(entity);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public void delete(long id){
		try {
			repository.deleteById(id);
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Drone> findAll(){		
			return repository.findAll();
	}
	
	public Drone findById(long id){
		try {
			Optional<Drone> optional = repository.findById(id);
			
			return optional.get();
		}catch(InternalError | Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
