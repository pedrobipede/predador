package br.com.maincore.predador.pageControl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maincore.predador.service.UsuarioService;

@Controller
@RequestMapping("/web/usuario")
public class UsuarioPageController {

	@Autowired
	private UsuarioService service;
	
	@GetMapping("gerenciamento")
	public String listar(Model model) {
		model.addAttribute("usuarios", service.findAll());
		
		return "usuarios/listarUsuarios";
	}
	
}
