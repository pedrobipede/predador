package br.com.maincore.predador.pageControl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomePageController {
	@RequestMapping
	public String home() {
		
		return "index";
	}
	
	@RequestMapping("login")
	public String login() {
		
		return "login/login";
	}
	
	@RequestMapping("register")
	public String register() {
		
		return "register/register";
	}
}
