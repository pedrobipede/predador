package br.com.maincore.predador.pageControl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web/rota")
public class RotaPageController {

	@RequestMapping("/montar")
	public String montarRota(Model model) {
		
		return "/maps/montarRota";
	}
	
	@RequestMapping("/gerenciamento")
	public String ativa(Model model) {
		
		return "/maps/rotas";
	}
}
