package br.com.maincore.predador.pageControl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maincore.predador.service.ModeloService;

@Controller
@RequestMapping("/web/modelo")
public class ModeloPageController {

	@Autowired
	private ModeloService service;
	
	@GetMapping("/gerenciamento")
	public String listar(Model model){
		
		model.addAttribute("modelos", service.findAll());
		
		return "modelos/listarModelos";
	}
}
