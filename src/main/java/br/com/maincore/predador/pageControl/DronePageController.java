	package br.com.maincore.predador.pageControl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maincore.predador.service.DroneService;
import br.com.maincore.predador.service.ModeloService;

@Controller
@RequestMapping("/web/drone")
public class DronePageController {
	
	@Autowired
	private DroneService service;
	
	@Autowired
	ModeloService serviceModel = new ModeloService();
	
	@RequestMapping("/gerenciamento")
	public String listar(Model model){
		
		
		model.addAttribute("drones", service.findAll());
		model.addAttribute("modelos", serviceModel.findAll());
		
		return "/drones/gerenciamento";
	}
	
	@GetMapping("/cadastrar")
	public String save(Model model) {
		return "/drones/cadastrar";
	}
	
	@RequestMapping("/localizacao")
	public String localizar(Model model) {
		
		
		return "/maps/localizacao";
	}
	
	@RequestMapping("/camera")
	public String camera() {
		
		return "/drones/camera";
	}
	
	@RequestMapping("/localizacaoAll")
	public String localizaçãoAll() {
		
		
		return "/maps/localizacaoAll";
	}
	
	@RequestMapping
	public String droneControle(Model model) {
		
	//	model.addAttribute("drones",this.service.findAll());
		
		return "/drones/menuDrone";
	}
	
	
}
