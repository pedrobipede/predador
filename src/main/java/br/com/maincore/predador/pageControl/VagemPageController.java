package br.com.maincore.predador.pageControl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.maincore.predador.model.Drone;
import br.com.maincore.predador.service.DroneService;

@Controller
@RequestMapping("/web/viagem")
public class VagemPageController {
	
	@Autowired
	private DroneService droneService;

	@RequestMapping("/viagemPage")
	public String controleViagens(Model model) {
		System.out.println("0");

		List<Drone> findAll = this.droneService.findAll();
		
		for (Drone drone : findAll) {
			System.out.println("além de bonito é cheroso : " + drone);

		}
		model.addAttribute("drones", findAll);
		return "drones/controleViagens";
	}
	
	@RequestMapping("/criarRota")
	public String montarRota() { 
		return "maps/montarRota";
	}
}
