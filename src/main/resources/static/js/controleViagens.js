var n = 0;
      var map;
      var marker;
      var infowindow;
      var messagewindow;
      var coordenadas =[];
     
      function initMap() {
        var california = {lat:-25.5054701, lng:-54.5712808 };
        map = new google.maps.Map(document.getElementById('map'), {
          center: california,
          zoom: 13
        });

        infowindow = new google.maps.InfoWindow({
          content: document.getElementById('form')
        });

        messagewindow = new google.maps.InfoWindow({
          content: document.getElementById('message')
        });

        google.maps.event.addListener(map, 'click', function(event) {
          
          alert(event.latLng);
         
          
          marker = new google.maps.Marker({
            position: event.latLng,
            map: map
          });


          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
          });
         
        });
          
      }

      function saveData() {
        var name = escape(document.getElementById('name').value);
        var address = escape(document.getElementById('address').value);
        var type = document.getElementById('type').value;
        var latlng = marker.getPosition();
        var url = 'phpsqlinfo_addrow.php?name=' + name + '&address=' + address +
                  '&type=' + type + '&lat=' + latlng.lat() + '&lng=' + latlng.lng();

        downloadUrl(url, function(data, responseCode) {

          if (responseCode == 200 && data.length <= 1) {
            infowindow.close();
            messagewindow.open(map, marker);
          }
        });
      }

      function downloadUrl(url, callback) {
    	  	var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request.responseText, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }
      function addMarker(position){
    	//  lat: , lng: position.data.lonjitude
    	  
    	  var latitude = parseFloat(position.data.latitude);
    	  var longitude = parseFloat(position.data.longitude);
    	  var myLatLng = {lat:latitude, lng:longitude};

    	  marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              title: 'Hello World!'
            });

      }
      function localizacao (serie) {
    	  var request = window.ActiveXObject ?
    	  new ActiveXObject('Microsoft.XMLHTTP') :
    	  new XMLHttpRequest;
    	  alert(serie.innerHTML);
  		request.onreadystatechange = function(){
  			if(this.readyState ==4 && this.status == 200){
  				alert(this.responseText);
  				var obj = JSON.parse(this.responseText);
  				alert(obj.data.latitude);
  				addMarker(obj);
  			}
  		}
  		request.open("GET","/api/drones/localizacaoAtual",true);
  		request.setRequestHeader("Content-type", "application/json");
  		request.send();
      }
