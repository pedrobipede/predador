 // This example creates an interactive map which constructs a polyline based on
 // user clicks. Note that the polyline only appears once its path property
 // contains two LatLng coordinates.

 var poly;
 var map;
 var coordenadas = [];
 var rota;


 function initMap() {
   map = new google.maps.Map(document.getElementById('map'), {
     zoom: 7,
     center: {
       lat: 41.879,
       lng: -87.624
     } // Center the map on Chicago, USA.
   });

   poly = new google.maps.Polyline({
     strokeColor: '#000000',
     strokeOpacity: 1.0,
     strokeWeight: 3
   });
   poly.setMap(map);

   // Add a listener for the click event
   map.addListener('click', addLatLng);
 }

 // Handles click events on a map, and adds a new point to the Polyline.
 function addLatLng(event) {
   var path = poly.getPath();

   // Because path is an MVCArray, we can simply append a new coordinate
   // and it will automatically appear.
   //    alert(coordenadas);
   path.push(event.latLng);
   // Add a new marker at the new plotted point on the polyline.
   var marker = new google.maps.Marker({
     position: event.latLng,
     title: '#' + path.getLength(),
     map: map
   });
   var latLng = marker.getPosition();
   var cordenadas = {
       latitude: latLng.lat(),
       longitude: latLng.lng()
   };
 //  var teste8 = JSON.stringify(cordenadas);

   //alert(teste8);
   coordenadas.push(cordenadas);

   var coordenadas2 = {
     coordenadas
   }
    rota = JSON.stringify(coordenadas2);

   alert(rota);

   //alert(coordenadas);
 }

 function salvarRota() {
   alert("oi");
   var request = window.ActiveXObject ?
     new ActiveXObject('Microsoft.XMLHTTP') :
     new XMLHttpRequest;
   //  alert(serie.innerHTML);
   request.onreadystatechange = function () {
     if (this.readyState == 4 && this.status == 200) {
       alert(this.responseText);
       var obj = JSON.parse(this.responseText);
       alert(obj);
     }
   }
   request.open("POST", "/api/rotas/salvar", true);
   request.setRequestHeader("Content-type", "application/json");
   request.send(rota);
 }