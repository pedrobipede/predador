(function ($) {
    'use strict';
    /*==================================================================
        [ Daterangepicker ]*/
    try {
        $('.js-datepicker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoUpdateInput": false,
            locale: {
                format: 'DD/MM/YYYY'
            },
        });

        var myCalendar = $('.js-datepicker');
        var isClick = 0;

        $(window).on('click', function () {
            isClick = 0;
        });

        $(myCalendar).on('apply.daterangepicker', function (ev, picker) {
            isClick = 0;
            $(this).val(picker.startDate.format('DD/MM/YYYY'));

        });

        $('.js-btn-calendar').on('click', function (e) {
            e.stopPropagation();

            if (isClick === 1) isClick = 0;
            else if (isClick === 0) isClick = 1;

            if (isClick === 1) {
                myCalendar.focus();
            }
        });

        $(myCalendar).on('click', function (e) {
            e.stopPropagation();
            isClick = 1;
        });

        $('.daterangepicker').on('click', function (e) {
            e.stopPropagation();
        });


    } catch (er) {
        console.log(er);
    }
    /*[ Select 2 Config ]
        ===========================================================*/

    try {
        var selectSimple = $('.js-select-simple');

        selectSimple.each(function () {
            var that = $(this);
            var selectBox = that.find('select');
            var selectDropdown = that.find('.select-dropdown');
            selectBox.select2({
                dropdownParent: selectDropdown
            });
        });

    } catch (err) {
        console.log(err);
    }


})(jQuery);


function salvarUsuario() {
    alert("teste");
    var ddd = document.getElementById("ddd");
    var cell1 = document.getElementById("cell");
    var cpf1 = document.getElementById("cpf");
    var email1 = document.getElementById("email");
    var endereco1 = document.getElementById("end");
    var login1 = document.getElementById("login");
    var nome1 = document.getElementById("nome");
    var sobrenome1 = document.getElementById("sobrenome");
    var senha1 = document.getElementById("senha");
    //  var tipo = document.getElementById("tipo");
    var tipo1 = parseInt("1",10);
    var fullname = nome1.value + " " + sobrenome1.value;
    var telefone = "(" + ddd.value + ") " + cell1.value;
    alert(cell1);
    var usuario = {
        tipo: tipo1,
        nome: fullname,
        endereco: endereco1.value,
        cpf: cpf1.value,
        celular: telefone,
        login: login1.value,
        senha: senha1.value,
        email:email1.value
    };
    alert("Depois do JSON");
    var usuarioJSON = JSON.stringify(usuario);
    alert(usuarioJSON);
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;
    //  alert(serie.innerHTML);
    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            alert(this.responseText);
            var obj = JSON.parse(this.responseText);
            alert(obj);
        }
    }
    alert("passo");
    request.open("POST", "/api/usuario/salvar", true);
    request.setRequestHeader("Content-type", "application/json");
    request.send(usuarioJSON);
}